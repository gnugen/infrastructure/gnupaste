{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RecordWildCards #-}

module Main (main) where

import           Protolude hiding (option)

import           Control.Monad.Logger
import           Database.Persist.Sqlite
import           Network.Wai.Handler.Warp (run)
import           Options.Applicative
import qualified Data.Text as T
import           Servant.Server
import           System.Directory
import           System.Posix.Files (setFileMode)

import Paste.API (PasteAPI)
import Paste.Handlers (server)
import Paste.Database (migrateAll, cleanupPastes)

data Arguments = Args
        { port        :: Int
        , sqlitePath  :: FilePath
        , pasteDir    :: FilePath
        , instanceUrl :: Text
        , createDB    :: Bool
        , debug       :: Bool
        }

-- argument parsing, see https://hackage.haskell.org/package/optparse-applicative
argParser :: Parser Arguments
argParser = Args
    <$> option auto
         ( long "port"
        <> short 'p'
        <> value 80
        <> metavar "PORT"
        <> help "HTTP port to listen on (defaults to 80)" )
    <*> strOption
         ( long "db-path"
        <> value "gnupaste.sqlite"
        <> metavar "DB"
        <> help "SQLite Database where data is stored (defaults to ./gnupaste.sqlite)" )
    <*> strOption
         ( long "paste-dir"
        <> value "pastes"
        <> metavar "DIR"
        <> help "Folder where the pastes will be stored (defaults to ./pastes)" )
    <*> option str
         ( long "instance-url"
        <> metavar "URL"
        <> help "The url of the instance (for example https://paste.gnugen.ch)" )
    <*> switch
         ( long "create-db"
        <> help "Create (or update) database schemas instead of running" )
    <*> switch
         ( long "debug"
        <> help "Verbose output, log sqlite queries" )

argParserInfo :: ParserInfo Arguments
argParserInfo = info (argParser <**> helper)
         ( fullDesc
        <> header "gnupaste - gnugen's pastebin" )

runServer :: Arguments -> IO ()
runServer Args{..} = do
        createDirectoryIfMissing True pasteDir
        setFileMode pasteDir 0o700
        pool <- logger $ createSqlitePool (T.pack sqlitePath) 1
        _ <- forkIO $ cleanupPastes pool (pasteDir<>"/")
        let srv = server pool (pasteDir<>"/") instanceUrl
            pasteAPI = Proxy :: Proxy PasteAPI
            application = serve pasteAPI srv
        run port application
    where logger = if debug then runStderrLoggingT
                            else (`runLoggingT` \_ _ _ _ -> return ())
                                        -- don't print anything

main :: IO ()
main = do
        Args{..} <- execParser argParserInfo
        let migrate = runSqlite (T.pack sqlitePath) $ do
                runMigration migrateAll
        if createDB then migrate else runServer Args{..}
