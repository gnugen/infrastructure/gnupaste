{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-|
Module      : Paste.Homepage
Description : This module contains the code to generate the HTML for the homepage.
Copyright   : (c) Joachim Desroches, 2018
License     : BSD3
Maintainer  : joachim.desroches@epfl.ch
Stability   : experimental
Portability : POSIX

This module contains the code for the generation of the homepage, written in the "Text.Blaze.Html5" EDSL.
-}

module Paste.Homepage where

import Protolude

import Data.FileEmbed
import Text.Blaze.Html5            as H
import Text.Blaze.Html5.Attributes as A

import Paste.Styles

-- | The homepage that serves as an instruction manual, and a post page for
-- people using browsers.
homepage :: Text -> Html
homepage url = (docTypeHtml ! lang "en" $ do
    H.head $ do
             H.title "GnuPaste"
             H.style (toHtml stylesheet)
             H.meta ! A.name "viewport"
                    ! A.content "width=device-width, initial-scale=1"

    H.body $ do
             h1 "GnuPaste"

             h2 "Usage"
             code $ "<command> | curl -F 'file=@-' -F 'type=text/plain' "
                     <> toHtml url

             h2 "Details"
             p $ do
                H.div $ do
                    dt $ code "file"
                    dd $ do "The file that will be uploaded and served."
                            " This is the only required field."
                H.div $ do
                    dt $ code "type"
                    dd $ do "Force a different mime-type than the one recognized"
                            " by your user agent. You " >> em "need" >> " to specify"
                            " that when piping to curl or you will end up with "
                            code "application/octet-stream" >> "."
                H.div $ do
                    dt $ code "expire"
                    dd $ do "The desired expiration date. This field defaults to one year."
                            " Valid units are "
                            strong "s" >> "(econds), "
                            strong "h" >> "(ours), "
                            strong "d" >> "(ays), "
                            strong "w" >> "(eeks) and "
                            strong "m" >> "(onths)."
             H.small $ do
                  "This service is provided with no guarantees of any kind. Please note"
                  " that your IP address will be stored for security purposes."

             h2 "Upload form"
             H.form ! A.method "POST" $ do
                table $ do
                    tr $ do
                         td $ H.label ! A.for "file" $ "Select file to upload"
                         td $ input ! A.type_ "file" ! A.id "file" ! A.name "file" ! A.required "file"
                    tr $ do
                         td $ H.label ! A.for "type" $ "Mime Type"
                         td $ input ! A.type_ "text" ! A.id "type" ! A.name "type"
                    tr $ do
                         td $ H.label ! A.for "expire" $ "Expiration date"
                         td $ input ! A.type_ "text" ! A.id "expire" ! A.name "expire"
                                    ! A.placeholder "365d" ! A.pattern "[1-9][0-9]*[shdwm]?"
                    tr $ do
                         td ""
                         td $ input ! A.type_ "submit" ! A.formenctype "multipart/form-data"
             footer $ do
                 p $ do
                      a ! href "https://gitlab.gnugen.ch/gnugen/gnupaste" $ "GnuPaste 2.1"
                      ", BSD3 licenced. Written in "
                      a ! href "https://haskell.org" $ "haskell"
                      " using "
                      a ! href "https://hackage.haskell.org/package/servant" $ "servant"
                      " by J. Desroches and A. Fontaine for GNU Generation, 2018 and 2020."
                 p $ do
                      "Built on the old paste program in Yesod by A. Angel, 2014."

             homepageJS

  ) >> preEscapedToHtml ("\n\n<!-- https://gitlab.gnugen.ch/gnugen/gnupaste/ -->"::Text)

homepageJS :: Html
homepageJS = script $ toHtml $ decodeUtf8 $(embedFile "src/Paste/homepage.js")
