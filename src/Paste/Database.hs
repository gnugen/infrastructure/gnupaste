{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE UndecidableInstances       #-}

{-|
Module      : Paste.Database
Description : gnupaste's functions to interact with the database
Copyright   : (c) Joachim Desroches, 2018
License     : BSD3
Maintainer  : joachim.desroches@epfl.ch
Stability   : experimental
Portability : POSIX

-}

module Paste.Database where

import           Protolude

import           Paste.Utils

import qualified Data.Text            as T
import           Data.Time.Clock
import           Database.Persist.Sql
import           Database.Persist.TH
import           System.Directory

-- Define schema and migration for our database, using template haskell.
share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Paste sql=pastes
    hash  Text
    mime  Text
    date  UTCTime
    dueAt UTCTime
    ipAdd Text
        UniqueHash hash
        deriving Show Read Eq
|]

-- | Insert a 'Paste' into the database connected to a pool.
insertPaste :: Paste            -- ^ The 'Paste' to input.
            -> ConnectionPool   -- ^ The 'ConnectionPool' to the database.
            -> IO ()
insertPaste = runSqlPool . insert_

-- | Generate a 4+ letter random hash, checking against the database for
-- unicity.
generateHash :: ConnectionPool -- ^ The 'ConnectionPool' to the database.
             -> IO Text
generateHash pool = do
    hash <- forM [1 .. 4::Int] . const $ pickElem pasteChars
    checkHash pool hash

    -- Inner recursive function that appends a character upon each collision.
    where checkHash :: ConnectionPool -> [Char] -> IO Text
          checkHash pool hash = do
                mExists <- runSqlPool (selectFirst [PasteHash ==. T.pack hash] []) pool
                case mExists of Nothing -> return $ T.pack hash
                                Just _  -> pickElem pasteChars >>= checkHash pool . (: hash)

-- | Returns the mimetype of an entry
lookupFiletype :: ConnectionPool -- ^ The 'ConnectionPool' to the database.
               -> Text           -- ^ The hash of the file
               -> IO (Maybe Text)
lookupFiletype pool hash = do
        result <- runSqlPool (selectList [PasteHash ==. hash] []) pool
        return (pasteMime <$> entityVal <$> listToMaybe result)

-- | Function to cleanup expired pastes from the database and drive. Do not
-- call outside of the main loop: this function does not terminate!
cleanupPastes :: ConnectionPool -- ^ The 'ConnectionPool' to the database
              -> FilePath       -- ^ The 'FilePath' to the directory containing uploads.
              -> IO ()
cleanupPastes pool pasteDir = do
    -- Obtain the list of expired pastes.
    now   <- getCurrentTime
    files <- runSqlPool (selectList [PasteDueAt <. now] []) pool

    -- Remove the files and DB references.
    sequence_ $ (processFiles pool pasteDir) <$> files

    -- Wait and recurse.
    threadDelay $ 60 * 1000000 -- once every minute
    cleanupPastes pool pasteDir

    -- Remove files from the database and then remove them.
    where processFiles :: ConnectionPool -> FilePath -> Entity Paste -> IO ()
          processFiles pool pasteDir (Entity key paste) = do
            runSqlPool (delete key) pool
            removeFile (pasteDir ++ hash)
            putStrLn ("Creating paste " ++ hash)
                where hash = T.unpack (pasteHash paste)
