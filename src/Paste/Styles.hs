{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Paste.Styles (stylesheet) where

import Protolude hiding (div, sym, Text, (&))

import Clay
import Data.Text.Lazy (Text)

stylesheet :: Text
stylesheet = renderWith compact [] css

css :: Css
css = do
        "*" ? do
                sym margin (px 0)

        html ? do
                fontFamily ["Europa", "Helvetica"] [sansSerif]
                textRendering optimizeLegibility

        body ? do
                sym padding (px 5)
                sym2 margin (px 0) auto
                maxWidth (em 50)

        h2 ? do
                fontSize (em 1.375)

        h1 ? do
                fontSize (em 1.75)

        body ? div <? do
                display flex
                flexDirection row

                dt ? do
                        minWidth (em 4)
                        fontWeight bold
                        textAlign end
                        paddingRight (em 1)

        body ? code <? do
                display block
                whiteSpace preWrap

        body<>footer ? (p<>div<>code<>h1<>h2) <? do
                marginTop (em 1)

        a ? do
                color "#ff0000"

        form ? do
                table ? do
                        "border-spacing" -: ".25em .625em"
                        sym2 margin (px 0) auto
                td ? firstChild & do
                        textAlign end
                        verticalAlign middle

        footer ?
             do marginTop (em 1)
                "border-top" -: "solid 1px"
                fontSize (em (13/16))
                textAlign center
