{-# LANGUAGE NoImplicitPrelude #-}

{-|
Module      : Paste.Utils
Description : gnupaste's utility functions
Copyright   : (c) Joachim Desroches, 2018
License     : BSD3
Maintainer  : joachim.desroches@epfl.ch
Stability   : experimental
Portability : POSIX
-}

module Paste.Utils where

import Protolude

import qualified Data.ByteString as B
import           Data.List
import           System.IO.Error
import           System.Random

-- | List of available characters for creating the hash.
pasteChars :: [Char]
pasteChars = ['a'..'z'] ++ ['A'..'Z'] ++ ['0'..'9']

-- | Pick an random element from a list.
pickElem :: [a]  -- ^ The list to pick from.
         -> IO a
pickElem xs = (xs !!) <$> randomRIO (0, length xs - 1)

-- | Read the file if it exists, else return Nothing.
-- Useful for reading golden files.
-- Taken from https://hackage.haskell.org/package/tasty-silver-3.2.1/docs/src/Test.Tasty.Silver.Internal.html#readFileMaybe
readFileMaybe :: FilePath -> IO (Maybe B.ByteString)
readFileMaybe path = catchJust
    (\e -> if isDoesNotExistErrorType (ioeGetErrorType e) then Just () else Nothing)
    (Just <$> B.readFile path)
    (const $ return Nothing)
