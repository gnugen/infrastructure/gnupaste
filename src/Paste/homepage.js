
/*
 * This simple js code is used to display the default mimetype
 * of the selected file in the field
 */

file_field = document.getElementById("file");
mime_field = document.getElementById("type");


function update_mimefield() {
	type = file_field.files[0].type;
	if (type !== undefined) {
		mime_field.placeholder = type;
		mime_field.value = type; // could be annoying, but allows editing
	}
};

file_field.onchange = update_mimefield;
file_field.onload = update_mimefield;
